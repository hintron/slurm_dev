#!/bin/bash

if [ "$1" == "" ]; then
	echo "Checking devices from 0-9:"
	COUNT=9
else
	echo "Checking devices from 0-$1:"
	COUNT="$1"
fi

# Print off all the devices we have access to, and suppress errors
for (( i=0; i<=$COUNT; i++ )); do
	# Use `head -cX` to read exactly X bytes
	var=$(head -c26 < /dev/slurm${i})
	# var=$(head -c1 < /dev/slurm${i})
	if [ $? == 0 ]; then
		# # If only read 1 byte, say what it is
		# var="'${var}' came from /dev/slurm${i}!"

		var="${var}; $(grep -i cpus_allowed_list /proc/self/status)"
		var="${var}; CVD=$CUDA_VISIBLE_DEVICES"
		echo $var;
	else
		echo "Cannot access /dev/slurm${i}"
	fi
done

# `read` is no good because it reads 1 byte at a time until it sees a newline,
# or reads `-nX` bytes. So use `head -cX` instead to read a true chunk of X.

# https://stackoverflow.com/questions/4411014/how-to-get-only-the-first-ten-bytes-of-a-binary-file