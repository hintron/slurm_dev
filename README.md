# Slurm testing devices

Kernel module to create some devices in linux to do testing.

## Description
When loaded this module will create up to SLURM_NUM_DEVICES (10 by default)
character devices in /dev/slurm[0-9], with its associated /sys/class/slurm
interfaces.

The devices can be opened, read, and written. Actually only the read does
something meaningful: it endlessly returns 0xFF, just as /dev/zero does.

We could say this is a /dev/one at the moment.

## Installation

* make clean
* make

Need roots permissions:

* make load : Does an insmod and loads the module
* make unload : Does an rmmod to unload the module
* make install : Installs the module in the system
* make uninstall: Removes the module from the system

After a 'make install', load it permanently adding a file:

```
]# cat /etc/modules-load.d/slurm_dev.conf
slurm_dev
```
## Usage

When the module is loaded, check /proc/devices|grep slurm, /dev/slurm* and
/sys/class/slurm*. You can make use of all of these as regular devices.

### On the command line

    $ read var < /dev/slurm0
    $ echo var

### Test script

Use the included test script to easily see what devices you have access to.
Pass in the maximum device # to check, or check all 0-9 with no argument.
For example, in a 6-GPU setup:

    $ salloc --gpus=1
    salloc: Granted job allocation 24342
    [salloc job=24342]$ ~/slurm/slurm_dev/test.sh 5
    Checking devices from 0-5:
    Hello, world from slurm0; Cpus_allowed_list: 0,6; CVD=0
    Cannot access /dev/slurm1
    Cannot access /dev/slurm2
    Cannot access /dev/slurm3
    Cannot access /dev/slurm4
    Cannot access /dev/slurm5


## License
This code is academic and for testing purposes, licensed under GPLv3.
